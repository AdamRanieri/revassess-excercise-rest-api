import { Assessment } from "../src/entities";
import { AssessmentDAO } from "../src/DAOs/assessment-dao";
import { AssessmentDaoPostgres } from "../src/DAOs/assessment-dao-postgres";
import { runner } from "../src/connection";

const assessmentDAO: AssessmentDAO = new AssessmentDaoPostgres();

test("Get assessment by ID", async () => {
	const result = await assessmentDAO.getAssessmentById(1);
	expect(result.assessmentId).toBe(1);
});
test("Get all assessments", async () => {
	const result = await assessmentDAO.getAllAssessments();
	expect(result.length).toBeGreaterThanOrEqual(3);
});
test("Get assessments by exercise ID", async () => {
	const result = await assessmentDAO.getAllAssessmentsByExerciseId(1);
	expect(result[0].exerciseId).toBe(1);
});
test("Get an associates set of assessments by exercise ID", async () => {
	const result = await assessmentDAO.getAllAssessmentsByExerciseIdByEmail(1, "RevAssociate@mail.com");
	console.log(result);
	expect(result.length).toBe(3);
});

afterAll(async () => {
	await runner.end();
});
