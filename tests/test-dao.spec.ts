import { runner } from "../src/connection";
import { TestDAO } from "../src/DAOs/test-dao";
import { TestDaoPostgres } from "../src/DAOs/test-dao-postgres";
import { Test } from "../src/entities";

const testDAO: TestDAO = new TestDaoPostgres();
// "tests": [
// 		{ "testName": "create_user_account", "isSuccessful": true, "points": 10, "errorMessage": "SUCCESS" },
// 		{ "testName": "update_user_account", "isSuccessful": false, "points": 20, "errorMessage": "error at line 45" },
// 		{ "testName": "delete_user_account", "isSuccessful": false, "points": 15, "errorMessage": "error at line 876" }
// 	]

test("Get all tests", async () => {

	const tests:Test[] = await testDAO.getAllTests(1);
	console.log(tests);

	expect(tests.length).toBe(2);
});

afterAll(async () => {
	await runner.end();
});
