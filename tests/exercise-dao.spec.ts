import { Exercise } from "../src/entities";
import { ExerciseDAO } from "../src/DAOs/exercise-dao";
import { ExerciseDaoPostgres } from "../src/DAOs/exercise-dao-postgres";
import { runner } from "../src/connection";

const exerciseDAO: ExerciseDAO = new ExerciseDaoPostgres();

const exercise1: Exercise = new Exercise(0, "Test 1", 100, "https://gitlab.com/RevMaster/RevTestExample");

test("Create exercise", async () => {
	const result = await exerciseDAO.createExercise(exercise1);
	expect(result.exerciseId).not.toBe(0);
});

test("Get all exercises", async () => {
	let exercise4: Exercise = new Exercise(0, "Test 4", 100, "https://gitlab.com/RevMaster/RevTestExample4");
	let exercise5: Exercise = new Exercise(0, "Test 5", 100, "https://gitlab.com/RevMaster/RevTestExample5");
	let exercise6: Exercise = new Exercise(0, "Test 6", 100, "https://gitlab.com/RevMaster/RevTestExample6");

	await exerciseDAO.createExercise(exercise4);
	await exerciseDAO.createExercise(exercise5);
	await exerciseDAO.createExercise(exercise6);

	const exercises: Exercise[] = await exerciseDAO.getAllExercises();

	expect(exercises.length).toBeGreaterThanOrEqual(3);
});

test("Get exercise by ID", async () => {
	let exercise7: Exercise = new Exercise(0, "Test 7", 100, "https://gitlab.com/RevMaster/RevTestExample7");
	exercise7 = await exerciseDAO.createExercise(exercise7);

	let retrievedExercise: Exercise = await exerciseDAO.getExerciseById(exercise7.exerciseId);

	expect(exercise7.exerciseName).toBe(retrievedExercise.exerciseName);
});

test("Delete an exercise", async () => {
	let exercise8: Exercise = new Exercise(0, "Test 8", 100, "https://gitlab.com/RevMaster/RevTestExample8");
	exercise8 = await exerciseDAO.createExercise(exercise8);

	const result: Boolean = await exerciseDAO.deleteExercise(exercise8.exerciseId);
	expect(result).toBeTruthy();
});

afterAll(async () => {
	await runner.end();
});
