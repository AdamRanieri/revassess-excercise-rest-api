import { Client } from "pg";
// require('dotenv').config({path:'C:\\Users\\renwi\\Desktop\\Revature_Learning\\RevAssess\\revassess-excercise-rest-api\\app.env'});
require("dotenv").config({ path: "E:\\mainCodeRepo\\project-3\\revassess-excercise-rest-api\\app.env" });

export const runner = new Client({
	user: "postgres",
	password: process.env.DBPASSWORD,
	database: process.env.DATABASENAME,
	port: 5432,
	host: "34.133.167.218"
});
runner.connect();
