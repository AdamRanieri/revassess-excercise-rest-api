import { Exercise } from "../entities";

export interface ExerciseDAO {
	createExercise(exercise: Exercise): Promise<Exercise>;

	getExerciseById(exerciseId: number): Promise<Exercise>;

	getAllExercises(): Promise<Exercise[]>;

	deleteExercise(exerciseId: number): Promise<Boolean>;
}
