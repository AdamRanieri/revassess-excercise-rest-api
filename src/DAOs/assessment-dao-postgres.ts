import { Assessment } from "../entities";
import { AssessmentDAO } from "./assessment-dao";
import { runner } from "../connection";
import { MissingResourceError } from "../errors";

async function getScore(assessmentId: number) {
	const sql2: string = "select sum(points) from test where assessment_id = $1 and is_successful = $2";
	const values2 = [assessmentId, true];
	const result2 = await runner.query(sql2, values2);
	const sum: number = Number(result2.rows[0].sum) ?? 0;
	return sum;
}

export class AssessmentDaoPostgres implements AssessmentDAO {
	async getAssessmentById(assessmentId: number): Promise<Assessment> {
		const sql: string = "select * from assessment where assessment_id = $1";
		const values = [assessmentId];
		const result = await runner.query(sql, values);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The runner with id ${assessmentId} does not exist!`);
		}
		const row = result.rows[0];
		const score = await getScore(row.assessment_id);
		const theAssessment: Assessment = new Assessment(
			row.assessment_id,
			row.assessment_time,
			row.email,
			row.exercise_id,
			row.source_link,
			score
		);
		return theAssessment;
	}

	async getAllAssessments(): Promise<Assessment[]> {
		const sql: string = "select * from Assessment";
		const result = await runner.query(sql);
		const assessments: Assessment[] = [];
		for (const row of result.rows) {
			const score = await getScore(row.assessment_id);
			const newAssessment: Assessment = new Assessment(
				row.assessment_id,
				row.assessment_time,
				row.email,
				row.exercise_id,
				row.source_link,
				score
			);
			assessments.push(newAssessment);
		}
		return assessments;
	}

	async getAllAssessmentsByExerciseId(exerciseId: number): Promise<Assessment[]> {
		const sql: string = "select * from assessment where exercise_id = $1";
		const values = [exerciseId];
		const result = await runner.query(sql, values);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The runner with id ${exerciseId} does not exist!`);
		}
		const assessments: Assessment[] = [];
		for (const row of result.rows) {
			const score = await getScore(row.assessment_id);
			const newAssessment: Assessment = new Assessment(
				row.assessment_id,
				row.assessment_time,
				row.email,
				row.exercise_id,
				row.source_link,
				score
			);
			assessments.push(newAssessment);
		}
		return assessments;
	}

	async getAllAssessmentsByExerciseIdByEmail(exerciseId: number, email: string): Promise<Assessment[]> {
		const sql: string = "select * from assessment where email = $1 and exercise_id = $2";
		const values = [email, exerciseId];
		const result = await runner.query(sql, values);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The runner with email ${email} does not exist in exercise ID number ${exerciseId}!`);
		}
		const assessments: Assessment[] = [];
		for (const row of result.rows) {
			const score = await getScore(row.assessment_id);
			const newAssessment: Assessment = new Assessment(
				row.assessment_id,
				row.assessment_time,
				row.email,
				row.exercise_id,
				row.source_link,
				score
			);
			assessments.push(newAssessment);
		}
		return assessments;
	}
}
