import { Assessment } from "../entities";

export interface AssessmentDAO {
	getAssessmentById(assessmentId: number): Promise<Assessment>;

	getAllAssessments(): Promise<Assessment[]>;

	getAllAssessmentsByExerciseId(exerciseId: number): Promise<Assessment[]>;

	getAllAssessmentsByExerciseIdByEmail(exerciseId: number, email: string): Promise<Assessment[]>;
}
