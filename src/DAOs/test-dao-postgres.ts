import { Test } from "../entities";
import { TestDAO } from "./test-dao";
import { runner } from "../connection";
import { MissingResourceError } from "../errors";

export class TestDaoPostgres implements TestDAO {
	async getAllTests(assessmentId: number): Promise<Test[]> {
		const sql: string = "select * from test where assessment_id = $1";
		const value = [assessmentId];
		const result = await runner.query(sql, value);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The assessment with id ${assessmentId} does not exist!`);
		}
		const tests: Test[] = [];
		for (const row of result.rows) {
			const newTest: Test = new Test(row.test_id, row.test_name, row.is_successful, row.points, row.error_message, row.assessment_id);
			tests.push(newTest);
		}
		return tests;
	}
}
