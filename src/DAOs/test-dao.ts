import { Test } from "../entities";

export interface TestDAO {
	getAllTests(assessmentId: number): Promise<Test[]>;
}
