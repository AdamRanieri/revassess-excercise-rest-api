import { Exercise } from "../entities";
import { ExerciseDAO } from "./exercise-dao";
import { runner } from "../connection";
import { MissingResourceError } from "../errors";

export class ExerciseDaoPostgres implements ExerciseDAO {
	async createExercise(newExercise: Exercise): Promise<Exercise> {
		const sql: string = "insert into exercise(exercise_name, max_points, repo_link) values ($1,$2,$3) returning exercise_id";
		const values = [newExercise.exerciseName, newExercise.maxPoints, newExercise.repoLink];
		const result = await runner.query(sql, values);
		newExercise.exerciseId = result.rows[0].exercise_id;
		return newExercise;
	}

	async getExerciseById(exerciseId: number): Promise<Exercise> {
		const sql: string = "select * from exercise where exercise_id = $1";
		const values = [exerciseId];
		const result = await runner.query(sql, values);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The exercise with id ${exerciseId} does not exist!`);
		}
		const row = result.rows[0];
		const theExercise: Exercise = new Exercise(row.exercise_id, row.exercise_name, row.max_points, row.repo_link);
		return theExercise;
	}

	async getAllExercises(): Promise<Exercise[]> {
		const sql: string = "select * from Exercise";
		const result = await runner.query(sql);
		const exercises: Exercise[] = [];
		for (const row of result.rows) {
			const newExercise: Exercise = new Exercise(row.exercise_id, row.exercise_name, row.max_points, row.repo_link);
			exercises.push(newExercise);
		}
		return exercises;
	}

	async deleteExercise(exerciseId: number): Promise<Boolean> {
		const sql: string = "delete from exercise where exercise_id = $1";
		const values = [exerciseId];
		const result = await runner.query(sql, values);
		if (result.rowCount === 0) {
			throw new MissingResourceError(`The exercise with id ${exerciseId} does not exist`);
		}
		return true;
	}
}
