export class Exercise {
	constructor(public exerciseId: number, public exerciseName: string, public maxPoints: number, public repoLink: string) {}
}

export class Assessment {
	constructor(
		public assessmentId: number,
		public assessmentTime: BigInt,
		public email: string,
		public exerciseId: number,
		public sourceLink: string,
		public score: number
	) {}
}

export class Test {
	constructor(
		public testId: number,
		public testName: string,
		public isSuccessful: boolean,
		public points: number,
		public errorMessage: string,
		public assessmentId: number
	) {}
}
