import { Test } from "../entities";

export interface TestService {
	//Reads
	allAssessmentTests(assessmentId: number): Promise<Test[]>;
}
