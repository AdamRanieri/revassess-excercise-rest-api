import { AssessmentDAO } from "../DAOs/assessment-dao";
import { AssessmentDaoPostgres } from "../DAOs/assessment-dao-postgres";
import { Assessment } from "../entities";
import { AssessmentService } from "./assessment-service";

export default class AssessmentServiceImpl implements AssessmentService {
	assessmentDAO: AssessmentDAO = new AssessmentDaoPostgres();

	async allAssessments(): Promise<Assessment[]> {
		const result = await this.assessmentDAO.getAllAssessments();
		return result;
	}
	async assessmentByID(assessmentId: number): Promise<Assessment> {
		const result = await this.assessmentDAO.getAssessmentById(assessmentId);
		return result;
	}
	async assessmentsByExerciseId(exerciseId: number): Promise<Assessment[]> {
		const result = await this.assessmentDAO.getAllAssessmentsByExerciseId(exerciseId);
		return result;
	}
	async assessmentsByExerciseIdByEmail(exerciseId: number, email: string): Promise<Assessment[]> {
		const result = await this.assessmentDAO.getAllAssessmentsByExerciseIdByEmail(exerciseId, email);
		return result;
	}
	async assessmentsByExerciseIdByRecent(exerciseId: number): Promise<Assessment[]> {
		const result = await this.assessmentDAO.getAllAssessmentsByExerciseId(exerciseId);
		const mapUni: Assessment[] = [
			...new Map(result.sort((a, b) => Number(a.assessmentTime) - Number(b.assessmentTime)).map((i) => [i["email"], i])).values()
		];
		return mapUni;
	}
	async assessmentsByExerciseIdByMax(exerciseId: number): Promise<Assessment[]> {
		const result = await this.assessmentDAO.getAllAssessmentsByExerciseId(exerciseId);
		const mapUni: Assessment[] = [...new Map(result.sort((a, b) => a.score - b.score).map((i) => [i["email"], i])).values()];

		return mapUni;
	}
}
