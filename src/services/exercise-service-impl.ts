import { Exercise } from "../entities";
import { ExerciseDaoPostgres } from "../DAOs/exercise-dao-postgres";
import { ExerciseService } from "./exercise-service";
import { ExerciseDAO } from "../DAOs/exercise-dao";

export default class ExerciseServiceImpl implements ExerciseService {
	exerciseDao: ExerciseDAO = new ExerciseDaoPostgres();

	async addExercise(exercise: Exercise): Promise<Exercise> {
		const result = await this.exerciseDao.createExercise(exercise);
		return result;
	}
	async exerciseByID(exerciseId: number): Promise<Exercise> {
		const result = await this.exerciseDao.getExerciseById(exerciseId);
		return result;
	}
	async allExercises(): Promise<Exercise[]> {
		const result = await this.exerciseDao.getAllExercises();
		return result;
	}
	async removeExercise(exerciseId: number): Promise<Boolean> {
		const result = await this.exerciseDao.deleteExercise(exerciseId);
		return result;
	}
}
