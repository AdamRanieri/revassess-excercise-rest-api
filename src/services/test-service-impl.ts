import { TestDAO } from "../DAOs/test-dao";
import { TestDaoPostgres } from "../DAOs/test-dao-postgres";
import { Test } from "../entities";
import { TestService } from "./test-service";

export default class TestServiceImpl implements TestService {
	testDao: TestDAO = new TestDaoPostgres();

	async allAssessmentTests(assessmentId: number): Promise<Test[]> {
		const result = await this.testDao.getAllTests(assessmentId);
		return result;
	}
}
