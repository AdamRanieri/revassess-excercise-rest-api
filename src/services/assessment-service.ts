import { Assessment } from "../entities";

export interface AssessmentService {
	// Reads
	allAssessments(): Promise<Assessment[]>;

	assessmentByID(assessmentId: number): Promise<Assessment>;

	assessmentsByExerciseId(exerciseId: number): Promise<Assessment[]>;

	assessmentsByExerciseIdByEmail(exerciseId: number, email: string): Promise<Assessment[]>;

	// Additional services for max score and recent score
	assessmentsByExerciseIdByMax(exerciseId: number): Promise<Assessment[]>;

	assessmentsByExerciseIdByRecent(exerciseId: number): Promise<Assessment[]>;
}
