import { Exercise } from "../entities";

export interface ExerciseService {
	// Create
	addExercise(exercise: Exercise): Promise<Exercise>;

	// Reads
	exerciseByID(exerciseId: number): Promise<Exercise>;

	allExercises(): Promise<Exercise[]>;

	// Deletes
	removeExercise(exerciseId: number): Promise<Boolean>;
}
