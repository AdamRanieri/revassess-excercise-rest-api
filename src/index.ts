import cors from "cors";
import express from "express";
import { Assessment, Test, Exercise } from "./entities";
import { MissingResourceError } from "./errors";
import { AssessmentService } from "./services/assessment-service";
import AssessmentServiceImpl from "./services/assessment-service-impl";
import { ExerciseService } from "./services/exercise-service";
import ExerciseServiceImpl from "./services/exercise-service-impl";
import { TestService } from "./services/test-service";
import TestServiceImpl from "./services/test-service-impl";
const middleware = require("./middleware");

const app = express();
app.use(express.json());
app.use(cors());

const assessmentService: AssessmentService = new AssessmentServiceImpl();
const testService: TestService = new TestServiceImpl();
const exerciseService: ExerciseService = new ExerciseServiceImpl();

// Verify for ingestion *NOTE* no security require

app.get("/exercises/:id/verify", async (req, res) => {
	try {
		const exerciseId = Number(req.params.id);
		await exerciseService.exerciseByID(exerciseId);
		res.send(true);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(false);
		}
	}
});

// POST
// Only requires exercise post
app.post("/exercises", middleware, async (req, res) => {
	try {
		let exercise: Exercise = req.body;
		exercise = await exerciseService.addExercise(exercise);
		res.status(201);
		res.send(exercise);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

// Gets
// Exercise requirements
app.get("/exercises", middleware, async (req, res) => {
	try {
		const exercises: Exercise[] = await exerciseService.allExercises();
		res.status(200);
		res.send(exercises);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

app.get("/exercises/:id", middleware, async (req, res) => {
	try {
		const exerciseId = Number(req.params.id);
		const exercise = await exerciseService.exerciseByID(exerciseId);
		res.send(exercise);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

// Assessments by Exercise requirements
app.get("/exercises/:id/assessments", middleware, async (req, res) => {
	try {
		const currentId = Number(req.params.id);
		const result = await assessmentService.assessmentsByExerciseId(currentId);
		res.status(200);
		res.send(result);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

app.get("/exercises/:id/assessments/:email", middleware, async (req, res) => {
	try {
		const currentId = Number(req.params.id);
		const theEmail = req.params.email;
		const result = await assessmentService.assessmentsByExerciseIdByEmail(currentId, theEmail);
		res.status(200);
		res.send(result);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

app.get("/exercises/:id/assess/recent", middleware, async (req, res) => {
	try {
		const currentId = Number(req.params.id);
		const result = await assessmentService.assessmentsByExerciseIdByRecent(currentId);
		res.status(200);
		res.send(result);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

app.get("/exercises/:id/assess/max", middleware, async (req, res) => {
	try {
		const currentId = Number(req.params.id);
		const result = await assessmentService.assessmentsByExerciseIdByMax(currentId);
		res.send(result);
		res.status(200);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

// Assessments requirements
app.get("/assessments", middleware, async (req, res) => {
	try {
		const assessments: Assessment[] = await assessmentService.allAssessments();
		res.send(assessments);
		res.status(200);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

app.get("/assessments/:id", middleware, async (req, res) => {
	try {
		const assessmentId = Number(req.params.id);
		const assessment = await assessmentService.assessmentByID(assessmentId);
		res.send(assessment);
		res.status(200);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

// Tests requirements
app.get("/assessments/:id/tests", middleware, async (req, res) => {
	try {
		const currentId = Number(req.params.id);
		const result = await testService.allAssessmentTests(currentId);
		res.send(result);
		res.status(200);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

// Deletes
// Only requires exercise delete
app.delete("/exercises/:id", middleware, async (req, res) => {
	try {
		const exerciseId = Number(req.params.id);
		await exerciseService.removeExercise(exerciseId);
		res.send(`${exerciseId} has been deleted.`);
	} catch (error) {
		if (error instanceof MissingResourceError) {
			res.status(404);
			res.send(error);
		}
	}
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("REST API started"));
