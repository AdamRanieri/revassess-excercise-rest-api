const jwt = require("jsonwebtoken");
const { Datastore } = require("@google-cloud/datastore");
const datastore = new Datastore();
import decrypt from "./decrypt";

module.exports = async (req, res, next) => {
	// check header or url parameters or post parameters for token
	var token = req.headers["token"];

	async function getSecret() {
		if (token) {
			const payload = jwt.decode(req.headers["token"]);
			const email = payload.email;
			const query = datastore.createQuery("Trainers").filter("email", "=", email);
			const [user] = await datastore.runQuery(query);

			return user[0].password;
		} else {
			return "No token";
		}
	}
	const pass = await getSecret();
	// decode token
	if (token) {
		// verifies secret and checks exp
		jwt.verify(token, decrypt(pass), function (err, decoded) {
			if (err) {
				return res.status(403).send({
					success: false,
					message: "Failed to authenticate token."
				});
			} else {
				// if everything is good, save to request for use in other routes
				req.decoded = decoded;
				next();
			}
		});
	} else {
		// if there is no token, return an error
		return res.status(403).send({
			success: false,
			message: "No token provided."
		});
	}
};
