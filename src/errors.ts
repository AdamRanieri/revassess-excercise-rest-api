export class MissingResourceError {
	message: string;
	description: string = "The resource could not be found";

	constructor(message: string) {
		this.message = message;
	}
}
