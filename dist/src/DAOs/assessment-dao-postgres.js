"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
exports.__esModule = true;
exports.AssessmentDaoPostgres = void 0;
var entities_1 = require("../entities");
var connection_1 = require("../connection");
var errors_1 = require("../errors");
function getScore(assessmentId) {
    var _a;
    return __awaiter(this, void 0, void 0, function () {
        var sql2, values2, result2, sum;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    sql2 = "select sum(points) from test where assessment_id = $1 and is_successful = $2";
                    values2 = [assessmentId, true];
                    return [4 /*yield*/, connection_1.runner.query(sql2, values2)];
                case 1:
                    result2 = _b.sent();
                    sum = (_a = Number(result2.rows[0].sum)) !== null && _a !== void 0 ? _a : 0;
                    return [2 /*return*/, sum];
            }
        });
    });
}
var AssessmentDaoPostgres = /** @class */ (function () {
    function AssessmentDaoPostgres() {
    }
    AssessmentDaoPostgres.prototype.getAssessmentById = function (assessmentId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, row, score, theAssessment;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "select * from assessment where assessment_id = $1";
                        values = [assessmentId];
                        return [4 /*yield*/, connection_1.runner.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The runner with id " + assessmentId + " does not exist!");
                        }
                        row = result.rows[0];
                        return [4 /*yield*/, getScore(row.assessment_id)];
                    case 2:
                        score = _a.sent();
                        theAssessment = new entities_1.Assessment(row.assessment_id, row.assessment_time, row.email, row.exercise_id, row.source_link, score);
                        return [2 /*return*/, theAssessment];
                }
            });
        });
    };
    AssessmentDaoPostgres.prototype.getAllAssessments = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sql, result, assessments, _a, _b, row, score, newAssessment, e_1_1;
            var e_1, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        sql = "select * from Assessment";
                        return [4 /*yield*/, connection_1.runner.query(sql)];
                    case 1:
                        result = _d.sent();
                        assessments = [];
                        _d.label = 2;
                    case 2:
                        _d.trys.push([2, 7, 8, 9]);
                        _a = __values(result.rows), _b = _a.next();
                        _d.label = 3;
                    case 3:
                        if (!!_b.done) return [3 /*break*/, 6];
                        row = _b.value;
                        return [4 /*yield*/, getScore(row.assessment_id)];
                    case 4:
                        score = _d.sent();
                        newAssessment = new entities_1.Assessment(row.assessment_id, row.assessment_time, row.email, row.exercise_id, row.source_link, score);
                        assessments.push(newAssessment);
                        _d.label = 5;
                    case 5:
                        _b = _a.next();
                        return [3 /*break*/, 3];
                    case 6: return [3 /*break*/, 9];
                    case 7:
                        e_1_1 = _d.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 9];
                    case 8:
                        try {
                            if (_b && !_b.done && (_c = _a["return"])) _c.call(_a);
                        }
                        finally { if (e_1) throw e_1.error; }
                        return [7 /*endfinally*/];
                    case 9: return [2 /*return*/, assessments];
                }
            });
        });
    };
    AssessmentDaoPostgres.prototype.getAllAssessmentsByExerciseId = function (exerciseId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, assessments, _a, _b, row, score, newAssessment, e_2_1;
            var e_2, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        sql = "select * from assessment where exercise_id = $1";
                        values = [exerciseId];
                        return [4 /*yield*/, connection_1.runner.query(sql, values)];
                    case 1:
                        result = _d.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The runner with id " + exerciseId + " does not exist!");
                        }
                        assessments = [];
                        _d.label = 2;
                    case 2:
                        _d.trys.push([2, 7, 8, 9]);
                        _a = __values(result.rows), _b = _a.next();
                        _d.label = 3;
                    case 3:
                        if (!!_b.done) return [3 /*break*/, 6];
                        row = _b.value;
                        return [4 /*yield*/, getScore(row.assessment_id)];
                    case 4:
                        score = _d.sent();
                        newAssessment = new entities_1.Assessment(row.assessment_id, row.assessment_time, row.email, row.exercise_id, row.source_link, score);
                        assessments.push(newAssessment);
                        _d.label = 5;
                    case 5:
                        _b = _a.next();
                        return [3 /*break*/, 3];
                    case 6: return [3 /*break*/, 9];
                    case 7:
                        e_2_1 = _d.sent();
                        e_2 = { error: e_2_1 };
                        return [3 /*break*/, 9];
                    case 8:
                        try {
                            if (_b && !_b.done && (_c = _a["return"])) _c.call(_a);
                        }
                        finally { if (e_2) throw e_2.error; }
                        return [7 /*endfinally*/];
                    case 9: return [2 /*return*/, assessments];
                }
            });
        });
    };
    AssessmentDaoPostgres.prototype.getAllAssessmentsByExerciseIdByEmail = function (exerciseId, email) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, assessments, _a, _b, row, score, newAssessment, e_3_1;
            var e_3, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        sql = "select * from assessment where email = $1 and exercise_id = $2";
                        values = [email, exerciseId];
                        return [4 /*yield*/, connection_1.runner.query(sql, values)];
                    case 1:
                        result = _d.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The runner with email " + email + " does not exist in exercise ID number " + exerciseId + "!");
                        }
                        assessments = [];
                        _d.label = 2;
                    case 2:
                        _d.trys.push([2, 7, 8, 9]);
                        _a = __values(result.rows), _b = _a.next();
                        _d.label = 3;
                    case 3:
                        if (!!_b.done) return [3 /*break*/, 6];
                        row = _b.value;
                        return [4 /*yield*/, getScore(row.assessment_id)];
                    case 4:
                        score = _d.sent();
                        newAssessment = new entities_1.Assessment(row.assessment_id, row.assessment_time, row.email, row.exercise_id, row.source_link, score);
                        assessments.push(newAssessment);
                        _d.label = 5;
                    case 5:
                        _b = _a.next();
                        return [3 /*break*/, 3];
                    case 6: return [3 /*break*/, 9];
                    case 7:
                        e_3_1 = _d.sent();
                        e_3 = { error: e_3_1 };
                        return [3 /*break*/, 9];
                    case 8:
                        try {
                            if (_b && !_b.done && (_c = _a["return"])) _c.call(_a);
                        }
                        finally { if (e_3) throw e_3.error; }
                        return [7 /*endfinally*/];
                    case 9: return [2 /*return*/, assessments];
                }
            });
        });
    };
    return AssessmentDaoPostgres;
}());
exports.AssessmentDaoPostgres = AssessmentDaoPostgres;
