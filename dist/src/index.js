"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var cors_1 = __importDefault(require("cors"));
var express_1 = __importDefault(require("express"));
var errors_1 = require("./errors");
var assessment_service_impl_1 = __importDefault(require("./services/assessment-service-impl"));
var exercise_service_impl_1 = __importDefault(require("./services/exercise-service-impl"));
var test_service_impl_1 = __importDefault(require("./services/test-service-impl"));
var middleware = require("./middleware");
var app = (0, express_1["default"])();
app.use(express_1["default"].json());
app.use((0, cors_1["default"])());
var assessmentService = new assessment_service_impl_1["default"]();
var testService = new test_service_impl_1["default"]();
var exerciseService = new exercise_service_impl_1["default"]();
// Verify for ingestion *NOTE* no security require
app.get("/exercises/:id/verify", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var exerciseId, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                exerciseId = Number(req.params.id);
                return [4 /*yield*/, exerciseService.exerciseByID(exerciseId)];
            case 1:
                _a.sent();
                res.send(true);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                if (error_1 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(false);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// POST
// Only requires exercise post
app.post("/exercises", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var exercise, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                exercise = req.body;
                return [4 /*yield*/, exerciseService.addExercise(exercise)];
            case 1:
                exercise = _a.sent();
                res.status(201);
                res.send(exercise);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                if (error_2 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_2);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// Gets
// Exercise requirements
app.get("/exercises", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var exercises, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, exerciseService.allExercises()];
            case 1:
                exercises = _a.sent();
                res.status(200);
                res.send(exercises);
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                if (error_3 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_3);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/exercises/:id", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var exerciseId, exercise, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                exerciseId = Number(req.params.id);
                return [4 /*yield*/, exerciseService.exerciseByID(exerciseId)];
            case 1:
                exercise = _a.sent();
                res.send(exercise);
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                if (error_4 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_4);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// Assessments by Exercise requirements
app.get("/exercises/:id/assessments", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var currentId, result, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                currentId = Number(req.params.id);
                return [4 /*yield*/, assessmentService.assessmentsByExerciseId(currentId)];
            case 1:
                result = _a.sent();
                res.status(200);
                res.send(result);
                return [3 /*break*/, 3];
            case 2:
                error_5 = _a.sent();
                if (error_5 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_5);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/exercises/:id/assessments/:email", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var currentId, theEmail, result, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                currentId = Number(req.params.id);
                theEmail = req.params.email;
                return [4 /*yield*/, assessmentService.assessmentsByExerciseIdByEmail(currentId, theEmail)];
            case 1:
                result = _a.sent();
                res.status(200);
                res.send(result);
                return [3 /*break*/, 3];
            case 2:
                error_6 = _a.sent();
                if (error_6 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_6);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/exercises/:id/assess/recent", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var currentId, result, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                currentId = Number(req.params.id);
                return [4 /*yield*/, assessmentService.assessmentsByExerciseIdByRecent(currentId)];
            case 1:
                result = _a.sent();
                res.status(200);
                res.send(result);
                return [3 /*break*/, 3];
            case 2:
                error_7 = _a.sent();
                if (error_7 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_7);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/exercises/:id/assess/max", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var currentId, result, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                currentId = Number(req.params.id);
                return [4 /*yield*/, assessmentService.assessmentsByExerciseIdByMax(currentId)];
            case 1:
                result = _a.sent();
                res.send(result);
                res.status(200);
                return [3 /*break*/, 3];
            case 2:
                error_8 = _a.sent();
                if (error_8 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_8);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// Assessments requirements
app.get("/assessments", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var assessments, error_9;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, assessmentService.allAssessments()];
            case 1:
                assessments = _a.sent();
                res.send(assessments);
                res.status(200);
                return [3 /*break*/, 3];
            case 2:
                error_9 = _a.sent();
                if (error_9 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_9);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/assessments/:id", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var assessmentId, assessment, error_10;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                assessmentId = Number(req.params.id);
                return [4 /*yield*/, assessmentService.assessmentByID(assessmentId)];
            case 1:
                assessment = _a.sent();
                res.send(assessment);
                res.status(200);
                return [3 /*break*/, 3];
            case 2:
                error_10 = _a.sent();
                if (error_10 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_10);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// Tests requirements
app.get("/assessments/:id/tests", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var currentId, result, error_11;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                currentId = Number(req.params.id);
                return [4 /*yield*/, testService.allAssessmentTests(currentId)];
            case 1:
                result = _a.sent();
                res.send(result);
                res.status(200);
                return [3 /*break*/, 3];
            case 2:
                error_11 = _a.sent();
                if (error_11 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_11);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// Deletes
// Only requires exercise delete
app["delete"]("/exercises/:id", middleware, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var exerciseId, error_12;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                exerciseId = Number(req.params.id);
                return [4 /*yield*/, exerciseService.removeExercise(exerciseId)];
            case 1:
                _a.sent();
                res.send(exerciseId + " has been deleted.");
                return [3 /*break*/, 3];
            case 2:
                error_12 = _a.sent();
                if (error_12 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_12);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
var PORT = process.env.PORT || 3000;
app.listen(PORT, function () { return console.log("REST API started"); });
