"use strict";
exports.__esModule = true;
exports.runner = void 0;
var pg_1 = require("pg");
// require('dotenv').config({path:'C:\\Users\\renwi\\Desktop\\Revature_Learning\\RevAssess\\revassess-excercise-rest-api\\app.env'});
require("dotenv").config({ path: "E:\\mainCodeRepo\\project-3\\revassess-excercise-rest-api\\app.env" });
exports.runner = new pg_1.Client({
    user: "postgres",
    password: process.env.DBPASSWORD,
    database: process.env.DATABASENAME,
    port: 5432,
    host: "34.133.167.218"
});
exports.runner.connect();
