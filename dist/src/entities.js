"use strict";
exports.__esModule = true;
exports.Test = exports.Assessment = exports.Exercise = void 0;
var Exercise = /** @class */ (function () {
    function Exercise(exerciseId, exerciseName, maxPoints, repoLink) {
        this.exerciseId = exerciseId;
        this.exerciseName = exerciseName;
        this.maxPoints = maxPoints;
        this.repoLink = repoLink;
    }
    return Exercise;
}());
exports.Exercise = Exercise;
var Assessment = /** @class */ (function () {
    function Assessment(assessmentId, assessmentTime, email, exerciseId, sourceLink, score) {
        this.assessmentId = assessmentId;
        this.assessmentTime = assessmentTime;
        this.email = email;
        this.exerciseId = exerciseId;
        this.sourceLink = sourceLink;
        this.score = score;
    }
    return Assessment;
}());
exports.Assessment = Assessment;
var Test = /** @class */ (function () {
    function Test(testId, testName, isSuccessful, points, errorMessage, assessmentId) {
        this.testId = testId;
        this.testName = testName;
        this.isSuccessful = isSuccessful;
        this.points = points;
        this.errorMessage = errorMessage;
        this.assessmentId = assessmentId;
    }
    return Test;
}());
exports.Test = Test;
