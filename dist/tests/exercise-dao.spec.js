"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var entities_1 = require("../src/entities");
var exercise_dao_postgres_1 = require("../src/DAOs/exercise-dao-postgres");
var connection_1 = require("../src/connection");
var exerciseDAO = new exercise_dao_postgres_1.ExerciseDaoPostgres();
var exercise1 = new entities_1.Exercise(0, "Test 1", 100, "https://gitlab.com/RevMaster/RevTestExample");
test("Create exercise", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, exerciseDAO.createExercise(exercise1)];
            case 1:
                result = _a.sent();
                expect(result.exerciseId).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("Get all exercises", function () { return __awaiter(void 0, void 0, void 0, function () {
    var exercise4, exercise5, exercise6, exercises;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                exercise4 = new entities_1.Exercise(0, "Test 4", 100, "https://gitlab.com/RevMaster/RevTestExample4");
                exercise5 = new entities_1.Exercise(0, "Test 5", 100, "https://gitlab.com/RevMaster/RevTestExample5");
                exercise6 = new entities_1.Exercise(0, "Test 6", 100, "https://gitlab.com/RevMaster/RevTestExample6");
                return [4 /*yield*/, exerciseDAO.createExercise(exercise4)];
            case 1:
                _a.sent();
                return [4 /*yield*/, exerciseDAO.createExercise(exercise5)];
            case 2:
                _a.sent();
                return [4 /*yield*/, exerciseDAO.createExercise(exercise6)];
            case 3:
                _a.sent();
                return [4 /*yield*/, exerciseDAO.getAllExercises()];
            case 4:
                exercises = _a.sent();
                expect(exercises.length).toBeGreaterThanOrEqual(3);
                return [2 /*return*/];
        }
    });
}); });
test("Get exercise by ID", function () { return __awaiter(void 0, void 0, void 0, function () {
    var exercise7, retrievedExercise;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                exercise7 = new entities_1.Exercise(0, "Test 7", 100, "https://gitlab.com/RevMaster/RevTestExample7");
                return [4 /*yield*/, exerciseDAO.createExercise(exercise7)];
            case 1:
                exercise7 = _a.sent();
                return [4 /*yield*/, exerciseDAO.getExerciseById(exercise7.exerciseId)];
            case 2:
                retrievedExercise = _a.sent();
                expect(exercise7.exerciseName).toBe(retrievedExercise.exerciseName);
                return [2 /*return*/];
        }
    });
}); });
test("Delete an exercise", function () { return __awaiter(void 0, void 0, void 0, function () {
    var exercise8, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                exercise8 = new entities_1.Exercise(0, "Test 8", 100, "https://gitlab.com/RevMaster/RevTestExample8");
                return [4 /*yield*/, exerciseDAO.createExercise(exercise8)];
            case 1:
                exercise8 = _a.sent();
                return [4 /*yield*/, exerciseDAO.deleteExercise(exercise8.exerciseId)];
            case 2:
                result = _a.sent();
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, connection_1.runner.end()];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
