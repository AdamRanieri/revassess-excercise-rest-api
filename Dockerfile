FROM node

WORKDIR /app

COPY . /app/

RUN npm i

ENV PORT=3000 \
    DATABASENAME=revassess-db

CMD ["npm", "start"]